# The Eduprox API Proxy
## What's this?

Once upon a time, [Berzan.js](https://github.com/Newbytee/berzan/) could get away with only being a frontend for the
 then called Novasoftware schedule API. As Berzeliusskolan (Berzan) eventually
 switched over to Skola24, the schedule provider's new API, this was no longer
 the case due to how CORS works.

This is an API that proxies requests in order to bypass the CORS restrictions so
 that Berzan.js can utilise the new API. Originally this was written in
 JavaScript for Node.js, however due to personal interests and changes in
 hosting I decided to rewrite it in Rust. The original Node.js version can be
 found here: https://gitlab.com/Newbyte/berzanjs-api

Note that this is currently neither endorsed nor officially supported by any of 
 the APIs proxied. 

## Supported API proxies

### Skola24

Endpoints are available under `/skola24/v0/`

## Setting up and running yourself

### System software requirements

Running the Eduprox API Proxy yourself requires cargo and rustc. These can be
 installed via [Rustup](https://rustup.rs/). You also need the development files 
 for OpenSSL and pkg-config installed globally. How you acquire these depends on
 your operating system, but on most Linux distributions they should be carried
 by your package manager, usually under the names libssl-dev (Debian-based, e.g.
 Ubuntu), openssl-devel (Fedora), or openssl (Arch/Manjaro) and pkg-config,
 respectively. pkg-config is not necessary on macOS.
  
The code was originally written for Rust 1.44, but will most likely work well
 compiled with later versions too.

Note that while you -- of course -- can use this API proxy from Windows clients,
 running the proxy is for the time being only supported on Unix platforms.
 Contributions welcome!

### Configuration

Configuration can be done via these 3 environment variables:

`EDUPROX_API_ADDRESS`: specifies address to bind to

`EDUPROX_API_PORT`: specifies port to bind to

`EDUPROX_API_SOCKET_PATH`: specifies path to a Unix socket to bind to

 - If both IP the address and/or port environment variable(s) are/is set and
 Unix-socket related one, the Unix socket configuration will take presidence

 - Only IPv4 is supported at the moment (i.e., no IPv6)

## Running the server

Running the server is done via the `cargo run` command. 

This will bind to the default address and port, which is 127.0.0.1:3030. If you
 want to configure this, you can use the environment variables mentioned in the
 Configuration section, for example:
 
`EDUPROX_API_ADDRESS=192.168.1.63 EDUPROX_API_PORT=8080 cargo run`

Or

`EDUPROX_API_SOCKET_PATH=/tmp/eduprox.sock cargo run`

## License

The Eduprox API Proxy is licensed under the zlib/libpng License. A
 copy of it should have been provided with the source code, but should this not
 be the case you can access the license here:
 https://opensource.org/licenses/zlib
